global L
global n x y
global rc

rc = 0.2;
L = 1;
n = 10;
x = rand(n, 1);
y = rand(n, 1);

function w = wk(r)
  global rc
  A = 1;
  r = abs(r); r /= rc; r *= 2;
  if      r < 1; w = (r^2*(3*r-6)+4)/4;
  elseif  r < 2; w = -(r-2)^3/4;
  else           w = 0; endif
endfunction

function r = wrap(r, r0)
  global L
  dr = r - r0;
  if     abs(dr - L) < abs(dr); r -= L;
  elseif abs(dr + L) < abs(dr); r += L; endif
endfunction

function r = dist(i, j)
  global x y
  xi = x(i); yi = y(i);
  xj = x(j); yj = y(j);
  xj = wrap(xj, xi); yj = wrap(yj, yi);
  dx = xi - xj; dy = yi - yj;
  r = sqrt(dx^2 + dy^2);
endfunction

d = zeros(n, 1) # density
for i = 1:n; for j = 1:n
  if i == j; continue; endif
  r = dist(i, j);
  if r > rc; continue; endif
  d(i) += wk(r);
endfor; endfor

